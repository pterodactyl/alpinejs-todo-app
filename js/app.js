(function (window) {
	'use strict';

	const store = {
		todos: JSON.parse(localStorage.getItem('todo-store') || '[]'),
		save() {
			localStorage.setItem('todo-store', JSON.stringify(this.todos));
		},
	};

	window.TodoApp = () => ({
		filter: 'all',
		newTodo: '',

		...store,

		init() {
			this.$watch('todos', () => {
				this.save();
			});
		},

		addTodo() {
			if (this.newTodo.trim() === '') {
				return;
			}

			this.todos.push({
				id: Date.now(),
				body: this.newTodo,
				completed: false,
			});

			this.newTodo = '';
		},

		get active() {
			return this.todos.filter(todo => !todo.completed);
		},

		get completed() {
			return this.todos.filter(todo => todo.completed);
		},

		get isAllCompleted() {
			return this.completed.length === this.todos.length && this.completed.length > 0;
		},

		get filteredTodos() {
			return {
				all: this.todos,
				active: this.active,
				completed: this.completed,
			}[this.filter];
		},

		deleteCompleted() {
			this.todos = this.active;
		},

		handleTodoDelete(id) {
			this.todos = this.todos.filter(todo => todo.id != id);
		},

		toggleAll() {
			const hasAllCompleted = this.completed.length === this.todos.length;

			this.todos.forEach(todo => todo.completed = !hasAllCompleted);
		},
	});

	window.TodoItem = todo => ({
		editing: false,
		body: todo.body,

		startEdit() {
			this.editing = true;

			this.$nextTick(() => {
				this.$root.querySelector('input.edit').focus();
			});
		},

		finishEdit() {
			this.editing = false;

			if (this.body.trim() === '') {
				this.onTodoDelete();
			} else {
				todo.body = this.body;
			}
		},

		cancelEdit() {
			this.editing = false;
			this.body = todo.body;
		},

		onTodoDelete() {
			this.$dispatch('delete-todo', todo.id);
		},
	});
})(window);
